**Instrucciones de compilación y ejecuación**
Se incluye un makefile para cada uno de los directorios:

Para compilar tanto secuencial como paralelo: make

Para ejecutar el secuencial: make runSeq
Para ejecutar el paralelo: make runPar 
