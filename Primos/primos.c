#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc, char const *argv[]) {
  int j;
  int prime_ctr;
  int primos[500000];
  prime_ctr=1;
  primos[0]=2;
  int current=3;
  int valid=0;

  double durac;
  clock_t inicio, fin;

  inicio = clock();
  while (prime_ctr < 500000){
      for (j=0; j<prime_ctr; j++){
        //Revisa los primos que hay en el array
          if (current % primos[j] == 0){
              valid = 1;
              j=prime_ctr+1;
          }
          else{
            valid=0;
          }
      }
      if (valid == 0){
        primos[prime_ctr]=current;
        prime_ctr += 1; //Actualiza contador de primos
        printf("Cantidad de primos:%d\n",prime_ctr );
      }
      else{
        current += 2;
      }
  }
  for (j=0; j<prime_ctr; j++){
      printf("%d\n", primos[j]);
  }

  fin = clock();

  durac = ((double) (fin - inicio)) / CLOCKS_PER_SEC;

  printf("El programa tardó %f segundos en \n", durac);

  return 0;
}
