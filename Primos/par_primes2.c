#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

int main(int argc , char**  argv ){

  int columnas, source, filas, fila, tag;


  clock_t inicio, fin;
  double durac;

  int rank , root_process , ierr , size ;
  MPI_Status status ;
  int cont = 3;
  int terminar = 0;
  int ind = 5;
  int tag_p =1;


  int i, j, l;
  int cant = 500000  ;
  int rango= 100;

  int primos[rango];



  int Digito[2];
  int next[rango];
  int send[2];
  int root;

  int h = 0;
  int bandera = 0;
  int rec[2];
  inicio = clock();





  // Ingresar valores de vector A






  ierr = MPI_Init(&argc , &argv ) ;
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank ) ;
  ierr = MPI_Comm_size (MPI_COMM_WORLD, &size ) ;


  if (rank == 0 ){
    int procs = MPI_Comm_size (MPI_COMM_WORLD, &size ) ;

    procs =size;
    printf ("\n Iniciando ejecucion:  %d \n", procs);


    printf ("\n2 {0} ----> N:1 \n3 {0} ----> N: 2 \n5 {0} ----> N: 3\n7 {0} ----> N: 4\n");

    /*
    printf (" Numero: 2 \n");
    printf (" Numero: 3 \n");
    printf (" Numero: 5 \n");
    */



    int k;

    for(i=1; i< size; i++){
      if (i==1) {
         send[0]=10;
         send[1]=rango+10;
         ierr = MPI_Send(&send, 2, MPI_INT, tag_p, 1, MPI_COMM_WORLD);
         ind = 10 + rango;
      }else {
        send[0]=ind;
        send[1]=ind + rango;
        ierr = MPI_Send(&send, 2, MPI_INT, i, 1, MPI_COMM_WORLD);
        //printf (" Enviar %d a %d \n", primos[0], i);
        ind = ind + rango;
      }


    }



    while(terminar < procs-1){
      ierr = MPI_Recv(&next, rango, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      source = status.MPI_SOURCE;
      tag = status.MPI_TAG;
      if (tag == 1){
        cont = cont + next[0];
        //printf ("contador: %d \n", cont);//CONTROL
        if (cont<cant) {
          //primos[cont]=next[0];
          for (i = 1; i <= next[0]; i++) {
            printf ("%d {%d}\n", next[i], source);
          }

        }

        if (cont>=cant) {
          for (i = 1; i <= next[0]; i++) {
            printf ("%d {%d} ---> potencial rebase\n", next[i], source);
          }

        }

        ind = ind + rango;
        //printf ("%d \n", ind);//CONTROL
        send[0]=ind;
        send[1]=ind + rango;

        printf (" %d a %d \n", send[0], send[1]); //CONTROL
        if (cont<cant){
          //printf (" Enviar %d a %d \n", primos[0], source);
          ierr = MPI_Send(&send, 2, MPI_INT, source, 1, MPI_COMM_WORLD);
        }else{
          ierr = MPI_Send(&send, 2, MPI_INT, source, 0, MPI_COMM_WORLD);
          printf (" Señal de parada a %d \n", source);
        }
      }else{
        terminar ++;
      }
    }
    /*
    primos[0]=2;
    for(j=0; j<cant; j++){
      printf (" Numero: %d", primos[j]);
      printf ("\n");
    } */

  } else{
    //printf ("start \n");
    tag_p =1;
    while(tag_p==1){
      //printf ("listo para recibir \n");
      ierr = MPI_Recv(&rec, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

      tag_p = status.MPI_TAG;
      if(tag_p==1){

        //CALCULOS()
        h=0;
        l =0;
        //printf ("REC: %d a %d \n", rec[0], rec[1]);

        for (l = rec[0]; l < rec[1]; l++) {
          root= (int) sqrt(l);
          root = l;
          //printf ("Proc: %d  \n", l);
          for (i = 2; i < root; i++) {
            if (l%i==0){
              // Digito[1] = 0;
              // i=rec;
              //printf ("COMPUESTO: %d\n", l);
              break;
            }
          }
          if(l == i){
            h++;
            primos[0] = h;
            primos[h] = l;
          }

          // if (Digito[1] = 1){
          //   printf ("%d \n", rec);
          // }
        }
        ierr = MPI_Send(&primos, rango, MPI_INT, 0, 1, MPI_COMM_WORLD);



      } else {
        ierr = MPI_Send(&primos, rango, MPI_INT, 0, 0, MPI_COMM_WORLD);
        printf ("end {%d}  \n", rank);
      }




    }
  }




  ierr = MPI_Finalize() ;
  return 0 ;
}
