#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
  int a[100], b[100][100], c[100];
  int i, j, m, n, x, y, k;
  double durac;
  clock_t inicio, fin;

  /* Ingresar valores de vector A*/
  printf ("\n Ingrese el tamaño del vector A: ");
  scanf ("%d", &m);

  for (i = 0; i < m; i++){
    printf (" A[%d] = ", (i+1));
    a[i]=rand() % 25;
    printf("%d\n",a[i]);
    //scanf ("%d", &a[i]);
    printf ("\n");
  }


  /* Ingresar valores de matriz B*/
  printf ("\n Ingrese la cantidad de filas y columnas de la matriz:\n");
  scanf ("%d%d", &x, &y);


  for (i = 0; i < x; i++){
    for (j = 0; j < y; j++)
    {
      printf (" B[%d][%d] = ", (i+1), (j+1));
      b[i][j]=rand() % 25;
      printf("%d\n",b[i][j]);
      //scanf ("%d", &b[i][j]);
    }
    printf ("\n");
  }

  inicio = clock();
  /* Calculo de las entradas de la matriz C */

  for (i = 0; i < m; i++){
    c[i] = 0;
    for (j = 0; j < y; j++){
      c[i]+=a[i]*b[i][j];
    }

  }

  fin = clock();


  /* Output */
  printf ("\n El vector resultante es:\n");
  for (i = 0; i < m; i++){

    printf (" c[%d] = %d", (i+1), c[i]);

    printf ("\n");
  }

  durac = ((double) (fin - inicio)) / CLOCKS_PER_SEC;

  printf("El programa tardó %f segundos calculando el la matriz resultante \n", durac);

  return 0;
}
