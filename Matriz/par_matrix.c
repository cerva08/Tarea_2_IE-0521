#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc , char**  argv ){

  int columnas, source, filas, fila, tag;



  int rank , root_process , ierr , size ;
  MPI_Status status ;
  int cont = 0;
  int terminar = 1;
  int ind = 0;
  int tag_p =1;

  int i, j;


  int mat[100][100];
  int vec[100];

  int Digito[2];
  int next[2];

  int salida[100];



  // Ingresar valores de vector A

  filas =100;
  columnas = 100;


  for (i = 0; i < filas; i++){
    vec[i]=rand() % 15;
  }
  for (i = 0; i < filas; i++){
    for (j = 0; j < columnas; j++)
    {
      mat[i][j]=rand() % 15;
    }
  }




  ierr = MPI_Init(&argc , &argv ) ;
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank ) ;
  ierr = MPI_Comm_size (MPI_COMM_WORLD, &size ) ;
  if (rank == 0 ){
    int procs = MPI_Comm_size (MPI_COMM_WORLD, &size ) ;

    procs =size;
    printf ("\n Iniciando ejecucion:  %d \n", procs);

    /*

    //SE CREA LA MATRIZ Y EL VECTOR EN EL RANK 0 Y SE ENVIA A SLAVES
    filas =12;
    columnas = 12;

    for (i = 0; i < filas; i++){
      vec[i]=i;
    }
    for (i = 0; i < filas; i++){
      for (j = 0; j < columnas; j++)
      {
        mat[i][j]=j;
      }
    }

    for(i=1; i<MPI_Comm_size (MPI_COMM_WORLD, &size ); i++){
      ierr = MPI_Send(&i, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
      ind = i;
    }


    */
    int k;
    for(i=1; i< size; i++){
      k = i-1;
      ierr = MPI_Send(&k, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
      ind = k;
    }

    while(cont < procs-1){
      ierr = MPI_Recv(&next, 2, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      source = status.MPI_SOURCE;
      tag = status.MPI_TAG;
      if (tag == 1){
        salida[next[0]]=next[1];
        ind ++;
        if (ind < filas){
          ierr = MPI_Send(&ind, 1, MPI_INT, source, 1, MPI_COMM_WORLD);
        }else{
          ierr = MPI_Send(&ind, 1, MPI_INT, source, 0, MPI_COMM_WORLD);
        }
      }else{
        cont ++;
      }
    }
    printf ("\n El vector resultante es:\n");
    for (i = 0; i < filas; i++){

      printf (" resultado[%d] = %d", (i+1), salida[i]);
      printf ("\n");
    }

  } else{
    printf ("start \n");
    tag_p =1;
    while(tag_p==1){
      printf ("listo para recibir \n");
      ierr = MPI_Recv(&fila, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      int filaBuff = fila;
      printf ("\n recibido:  %d \n", fila);


      tag_p = status.MPI_TAG;
      if(tag_p==1){

        //CALCULOS()
        Digito[0]= fila;
        Digito[1]= 0;
        for (j = 0; j < columnas; j++){
          Digito[1]+=vec[j]*mat[j][fila];
        }
        //CALCULOS()
        ierr = MPI_Send(&Digito, 2, MPI_INT, 0, 1, MPI_COMM_WORLD);
        printf (" send[%d][%d] \n ", Digito[0] , Digito[1]);

      } else {
        ierr = MPI_Send(&Digito, 2, MPI_INT, 0, 0, MPI_COMM_WORLD);
          printf ("end \n");
      }




    }
  }




  ierr = MPI_Finalize() ;
  return 0 ;
}
