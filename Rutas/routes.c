#include<stdio.h>

int ary[10][10],completed[10],num_ciudades=8,cost=0;

void inputDists()
{
    int i,j;
    //int num_ciudades = 8;

    ary[0][0]= 0;
    ary[0][1]= 115;
    ary[0][2]= 8;
    ary[0][3]= 17;
    ary[0][4]= 167;
    ary[0][5]= 26;
    ary[0][6]= 83;
    ary[0][7]= 75;

    ary[1][0]= 115;
    ary[1][1]= 0;
    ary[1][2]= 120;
    ary[1][3]= 129;
    ary[1][4]= 272;
    ary[1][5]= 92;
    ary[1][6]= 197;
    ary[1][7]= 100;

    ary[2][0]= 8;
    ary[2][1]= 120;
    ary[2][2]= 0;
    ary[2][3]= 9;
    ary[2][4]= 160;
    ary[2][5]= 34;
    ary[2][6]= 78;
    ary[2][7]= 83;

    ary[3][0]= 17;
    ary[3][1]= 129;
    ary[3][2]= 9;
    ary[3][3]= 0;
    ary[3][4]= 151;
    ary[3][5]= 43;
    ary[3][6]= 69;
    ary[3][7]= 91;

    ary[4][0]= 167;
    ary[4][1]= 272;
    ary[4][2]= 160;
    ary[4][3]= 151;
    ary[4][4]= 0;
    ary[4][5]= 193;
    ary[4][6]= 98;
    ary[4][7]= 236;

    ary[5][0]= 26;
    ary[5][1]= 92;
    ary[5][2]= 34;
    ary[5][3]= 43;
    ary[5][4]= 193;
    ary[5][5]= 0;
    ary[5][6]= 108;
    ary[5][7]= 55;

    ary[6][0]= 83;
    ary[6][1]= 197;
    ary[6][2]= 78;
    ary[6][3]= 69;
    ary[6][4]= 98;
    ary[6][5]= 108;
    ary[6][6]= 0;
    ary[6][7]= 141;

    ary[7][0]= 75;
    ary[7][1]= 100;
    ary[7][2]= 83;
    ary[7][3]= 91;
    ary[7][4]= 236;
    ary[7][5]= 55;
    ary[7][6]= 141;
    ary[7][7]= 0;


    //Inicializa ciudades como NO visitadas
    for(i=0;i < num_ciudades;i++){
        completed[i]=0;
    }

    printf("\n\nLa lista de distancias es:");

    for( i=0;i < num_ciudades;i++){
        printf("\n");

        for(j=0;j < num_ciudades;j++)
            printf("\t%d",ary[i][j]);
    }
}

void small_cost(int city)
{
    int i,ncity;

    completed[city]=1; //Marca la ciudad como visitada

    if (city+1==1){
      printf("San Jose\n");
    }
    else if(city+1==2){
      printf("Limon\n");
    }
    else if(city+1==3){
      printf("San Francisco\n");
    }
    else if(city+1==4){
      printf("Alajuela\n");
    }
    else if(city+1==5){
      printf("Liberia\n");
    }
    else if(city+1==6){
      printf("Paraiso\n");
    }
    else if(city+1==7){
      printf("Puntarenas\n");
    }
    else if(city+1==8){
      printf("San Isidro\n");
    }
    
    ncity=least(city);

    if(ncity==999)
    {
        ncity=0;
        if (ncity+1==1){
          printf("San Jose\n");
        }
        else if(ncity+1==2){
          printf("Limon\n");
        }
        else if(ncity+1==3){
          printf("San Francisco\n");
        }
        else if(ncity+1==4){
          printf("Alajuela\n");
        }
        else if(ncity+1==5){
          printf("Liberia\n");
        }
        else if(ncity+1==6){
          printf("Paraiso\n");
        }
        else if(ncity+1==7){
          printf("Puntarenas\n");
        }
        else if(ncity+1==8){
          printf("San Isidro\n");
        }

        cost+=ary[city][ncity];

        return;
    }

    small_cost(ncity);
}

int least(int c)
{
    int i,nc=999;
    int min=999,kmin;

    for(i=0;i < num_ciudades;i++)
    {
        if((ary[c][i]!=0)&&(completed[i]==0))
            if(ary[c][i]+ary[i][c] < min)
            {
                min=ary[i][0]+ary[c][i];
                kmin=ary[c][i];
                nc=i;
            }
    }

    if(min!=999)
        cost+=kmin;

    return nc;
}

int main()
{
    inputDists();

    printf("\n\nRecorrido a tomar:\n");
    small_cost(0); //passing 0 because starting vertex

    printf("\n\nDistancia minima: %d\n ",cost);

    return 0;
}
