#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>



int main(int argc , char**  argv ){

  int columnas, source, filas, fila, tag;



  int rank , root_process , ierr , size ;
  MPI_Status status ;
  int cont = 0;
  int terminar = 1;
  int ind = 0;
  int tag_p =1;

  int i, j;
  int a,b,c,d,e,f,g;
  int ary[8][8];


  int mat[5040][7];
	int pesos[8][8];
  int vec[7] = {1,2,3,4,5,6,7};
  int ruta[7] = {0,0,0,0,0,0,0};
  int contRutas = 0;
	int vecTerminar[7] = {1,2,3,4,5,6,7};

  int Digito[2];
  int next;

  int salida[15];
	int contPermut=0;
	int one, two, three, four, five, six, seven, eight;
	int indMenor=0;

	for (one = 0; one < 8; one++) {
        for (two = 0; two < 8; two++) {
            if (two != one) {
                for (three = 0; three < 8; three++) {
                    if (one != three && two != three) {
                        for (four = 0; four < 8; four++)
                            if (one != four && two != four && three != four) {
															for (five = 0; five < 8; five++)
																	if (one != five && two != five && three != five && four != five) {
																		for (six = 0; six < 8; six++)
																				if (one != six && two != six && three != six && four != six && five != six) {
																					for (seven = 0; seven < 8; seven++)
																							if (one != seven && two != seven && three != seven && four != seven && five != seven && six != seven) {
																								for (eight = 0; eight < 8; eight++)
																										if (one != eight && two != eight && three != eight && four != eight && five != eight && six != eight && seven != eight) {
																												if(contPermut <= 5040){
																													for (i = 0; i<7; i++){
																														if (i==0) {
																															mat[contPermut][i]=two;
																														} else if (i==2){
																															mat[contPermut][i]=four;
																														} else if (i==3){
																															mat[contPermut][i]=five;
																														} else if (i==4){
																															mat[contPermut][i]=six;
																														} else if (i==5){
																															mat[contPermut][i]=seven;
																														} else if (i==6){
																															mat[contPermut][i]=eight;
																														} else if (i==1){
																															mat[contPermut][i]=three;
																														}
																													}
																													contPermut++;
																												}
																										}
																							}
																				}
																	}
                            }
                    }
                }
            }
        }
    }

  printf ("\n");
  for (i = 0; i < 8; i++){
    for (j = 0; j < 8; j++){
      ary[i][j]=rand() % 15;
      printf ("%d   ", ary[i][j]);
    }
    printf ("\n");
  }






  ierr = MPI_Init(&argc , &argv ) ;
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank ) ;
  ierr = MPI_Comm_size (MPI_COMM_WORLD, &size ) ;
  if (rank == 0 ){
    int procs = MPI_Comm_size (MPI_COMM_WORLD, &size ) ;

    procs =size;
    printf ("\n Iniciando ejecucion:  %d \n", procs);





    int k;
    for(i=1; i< size; i++){
      k = i-1;
      for (j=0; j<7; j++){
        ruta[j]=mat[k][j];
      }
      ierr = MPI_Send(&ruta, 7, MPI_INT, i, 1, MPI_COMM_WORLD);
      ind = k;
    }

    int menor = 999999;

    while(cont < procs-1){
      ierr = MPI_Recv(&next, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      source = status.MPI_SOURCE;
      tag = status.MPI_TAG;
      if (tag == 1){
        if (next < menor) {
          menor = next;
					indMenor = ind;
        }
        ind ++;
        for (i=0; i<7; i++){
          ruta[i]=mat[ind][i];
        }
        if (ind < 5040){
          ierr = MPI_Send(&ruta, 7, MPI_INT, source, 1, MPI_COMM_WORLD);
					//printf ("\n enviando:  %d \n", ind);
        }else{
          ierr = MPI_Send(&vecTerminar, 7, MPI_INT, source, 0, MPI_COMM_WORLD);
					//printf ("\n enviando (tag 0):  %d \n", ind);
        }
      }else{
        cont ++;
      }
    }

		 printf ("\n La Ruta mas corta es saliendo de San Jose y pasando por: 0");
		 for (i = 0; i < 7; i++) {
		 	printf (", %d",mat[indMenor][i]);
		 }
		 printf (", 0\n Con un valor de: %d\n", menor);


    // printf ("\n El vector resultante es:\n");
    // for (i = 0; i < filas; i++){
    //
    //   printf (" resultado[%d] = %d", (i+1), salida[i]);
    //   printf ("\n");
    // }

  } else{
    printf ("start \n");
    tag_p =1;
    while(tag_p==1){
      //printf ("listo para recibir \n");
      ierr = MPI_Recv(&ruta, 7, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);


      tag_p = status.MPI_TAG;
      if(tag_p==1){
        int suma = 0;
        int ciudad = 0;
        for(i=0; i<7; i++){
          suma = suma + ary[ciudad][ruta[i]];
          ciudad = i;
        }
        suma = suma + ary[ruta[7]][0];

        ierr = MPI_Send(&suma, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
        //printf (" send[%d][%d] \n ", Digito[0] , Digito[1]);

      } else {
        int suma=0;
        ierr = MPI_Send(&suma, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
          printf ("end \n");
      }




    }
  }




  ierr = MPI_Finalize() ;
  return 0 ;
}
